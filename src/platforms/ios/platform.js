game.module(
    'game.platform'
)
.require(
    'plugins.cocoon'
)
.body(function () {
    game.PLATFORM = 'ios';

    var advBottom = document.getElementById('add_bottom');

    game.addAdv = function() {
        setTimeout(function() {
            var el = document.createElement('script');
            el.type = 'text/javascript';
            el.src = '//ad.leadboltmobile.net/show_app_ad.js?section_id=508592791';
            el.async = true;
            document.getElementsByTagName('body')[0].appendChild(el);
            // advBottom.innerHTML = '<script type="text/javascript" src="http://ad.leadboltmobile.net/show_app_ad.js?section_id=955765861"></script>';
        }, 0);
    };

    game.clearAdv = function() {
        advBottom.innerHTML = '';
    };

    // var gc = Cocoon.Social.GameCenter;
    // var socialService = gc.getSocialInterface();
    // var loggedIn = false;

    // game.isSocial = !!socialService;

    // socialService.on('loginStatusChanged', function(loggedIn, error) {
    //     if (loggedIn) {
    //         console.log('Logged into social service');

    //         requestScore();
    //     }
    // });

    // function loginGameCenter() {
    //     socialService.login(function(loggedIn, error) {
    //         if (!loggedIn || error) {
    //             console.error('Login failed: ' + JSON.stringify(error));

    //             //Tell the user that Game Center is Disabled
    //             if (error.code == 2) {
    //                 // go to gamecenter app
    //                 Cocoon.Dialog.confirm({
    //                     title: 'Game Center Disabled',
    //                     message: 'Sign in with the Game Center application to enable it',
    //                     confirmText: 'Ok',
    //                     cancelText: 'Cancel'
    //                 }, function(accepted) {
    //                     accepted ? Cocoon.App.openURL('gamecenter:') : game.showStartPanel();
    //                 });
    //             }
    //         } else {
    //             requestScore();
    //         }
    //     });
    // };

    // function requestScore() {
    //     socialService.requestScore(function(score, error) {
    //         console.log(JSON.stringify(score))
    //         if (error) {
    //             console.error('Error getting user score: ' + error.message);
    //         } else if (score) {
    //             console.log('score: ' + score.score);
    //             game.STATS.BEST = score.score;
    //         }

    //         game.showStartPanel();
    //     }, {
    //         leaderboardID: 'helpthemoon1'
    //     });
    // };

    // game.updateStorage = function(best) {
    //     socialService.submitScore(best, function(err) {
    //         console.log(err)
    //     });
    // };

    // game.getBestScore = function(cb) {
    //     loggedIn ? requestScore() : loginGameCenter();
    // };

    game.updateStorage = function(best) {
        game.storage.set('best', best);
    };

    game.getBestScore = function(cb) {
        game.STATS.BEST = game.storage.get('best', 0);
        game.showStartPanel();
    };
});
