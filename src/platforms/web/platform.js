game.module(
    'game.platform'
)
.require(
    'plugins.utils'
)
.body(function () {
    game.PLATFORM = 'web';

    game.updateStorage = function(best) {
        game.storage.set('best', best);
    };

    game.getBestScore = function() {
        game.STATS.BEST = game.storage.get('best', 0);
        game.showStartPanel();
    };
});
