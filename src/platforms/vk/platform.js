game.module(
    'game.platform'
)
.require(
    'plugins.utils',
    'plugins.parse'
)
.body(function () {
    game.PLATFORM = 'vk';
    game.VK = VK;

    var API_PHP_URL = '//api-instaflat.rhcloud.com/moon_upload.php';
    var VK_USER_ID = game.utils.getHashVal('viewer_id');
    var VK_APP_URL = 'https://vk.com/moonhelp';

    console.log('VK UID: ', VK_USER_ID);

    game.vk = {
        showInviteBox: function() {
            game.VK.callMethod('showInviteBox');
        },
        uploadWallPhoto: function() {
            $('.paranja').show();

            VK.api('photos.getWallUploadServer', {}, function(res) {
                $.ajax({
                    url: API_PHP_URL,
                    jsonp: 'callback',
                    dataType: 'jsonp',
                    data: {
                        photo_id: 900,
                        photo_url: 'http://pp.vk.me/c623726/v623726483/36ad2/a7ExJYlpAjo.jpg',
                        upload_url: res.response.upload_url
                    },
                    success: function(response) {
                        VK_USER_ID && (response.user_id = VK_USER_ID);

                        VK.api('photos.saveWallPhoto', response, function(res) {
                            if (res.error || !res.response) return;

                            var params = res.response[0];
                            var sendParams = {
                                owner_id: params.owner_id,
                                message: 'Мой текущий счет: ' + game.STATS.SCORE + ', лучший: ' + game.STATS.BEST
                                    + ', в игре "Помоги Луне" ' + VK_APP_URL,
                                attachments: (params.id || ('photo' + params.owner_id + '_' + params.pid)) + ',' + VK_APP_URL
                            };

                            VK.api('wall.post', sendParams, function(res) {
                                $('.paranja').hide();

                                if (res.error || !res.response) {
                                    return;
                                };

                                console.log('VK: Wall post saved!');
                                Parse.Analytics.track('wall_share_result');
                            });
                        });
                    }
                });
            });
        }
    };

    game.updateStorage = function(best) {
        game.storage.set('best', best);

        game.VK.api('storage.set', { key: 'best', value: best }, function(res) {
            console.log('New best score!');
        });

        var VK = game.Parse.Object.extend('VK');
        var query = new game.Parse.Query(VK);
        query.equalTo('uid', game.VK_USER_ID);

        query.first().then(function(userScore) {
            if (userScore) {
                userScore.save({ score: best }).then(function() {
                    console.log('Parse: new score saved!');
                });
            } else {
                var gameScore = new VK();

                gameScore.save({ score: best, uid: game.VK_USER_ID }).then(function(argument) {
                    console.log('Parse: new score saved!');
                });
            }
        }, function() {
            console.log('Parse error!');
        });
    };

    game.getBestScore = function() {
        game.VK.api('storage.get', { key: 'best' }, function(res) {
            res.response.length && (game.STATS.BEST = parseInt(res.response, 10));
            console.log('BEST: ', game.STATS.BEST);

            game.showStartPanel();
        }.bind(this));
    };
});
