game.module(
    'plugins.utils'
)
.body(function () {
    game.utils = {
        getHashVal: function(key) {
            var vars, varsLength, pair;

            vars = location.search.slice(1).split('&');
            varsLength = vars.length;

            for (var i = 0; i < varsLength; i++) {
                pair = vars[i].split('=');
                if (pair[0] === key) return pair[1];
            }
        }
    };
});
