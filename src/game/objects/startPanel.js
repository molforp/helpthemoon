game.module(
    'game.objects.startPanel'
)
.body(function() {
    game.createClass('StartPanel', {
        init: function() {
            this.container = new game.Container().addTo(game.scene.stage);
            this.halfWidth = game.system.width / 2;
            this.halfHeight = game.system.height / 2;

            // var places = new game.Button('endPlaces',
            //     game.system.width - 40 * game.scale,
            //     game.system.height - 30 * game.scale,
            //     this.container
            // );

            // places.sprite.scale = { x: 0.9, y: 0.9 };
            // places.sprite.mouseover = scale.bind(places, 1);
            // places.sprite.mouseout = scale.bind(places, 0.9);

            // places.sprite.click = places.sprite.tap = function() {
            //     game.showTopPlaces();
            // };

            game.isVk && this.addPlusButton();
            this.addLogo();
            this.addHowTo();

            // game.addAdv();
        },

        addBest: function() {
            var bestScoreText = new game.BitmapText(
                game.trans('best') + ': ' + game.STATS.BEST.toString(),
                { font: '18px font' }
            );

            bestScoreText.position.x = 18 * game.scale;
            bestScoreText.position.y = game.system.height - 80 * game.scale;

            this.container.addChild(bestScoreText);
        },

        addLogo: function() {
            var logo = new game.Sprite('logo');

            logo.position.set(this.halfWidth, 90 * game.scale);
            logo.anchor = { x: 0.5, y: 0.5 };

            game.scene.addTween(logo.position, {
                y: 130 * game.scale
            }, 1800, {
                repeat: Infinity,
                yoyo: true,
                easing: 'Quadratic.InOut'
            }).start();

            this.container.addChild(logo);
        },

        addArrowText: function() {
            var arrowText = new game.Text('← →', { font: 25 * game.scale + 'px Tahoma', fill: 'white' });
            var orText = new game.BitmapText(game.trans('or'), { font: '15px font' });

            orText.position.set(this.halfWidth - orText.textWidth / 2, this.halfHeight + 75 * game.scale);
            arrowText.position.set(this.halfWidth - arrowText.width / 2, this.halfHeight + 85 * game.scale);

            this.container.addChild(arrowText);
            this.container.addChild(orText);
        },

        addHowTo: function() {
            var arrowRight = new game.Sprite('arrowRight');
            var arrowLeft = new game.Sprite('arrowRight');
            var box = new game.Sprite('boxHowTo');

            var rightRect = new game.Graphics();
            rightRect.beginFill(0xffffff);
            rightRect.alpha = 0;
            rightRect.drawRect(this.halfWidth, 0, this.halfWidth, game.system.height);

            var leftRect = new game.Graphics();
            leftRect.beginFill(0xffffff);
            leftRect.alpha = 0;
            leftRect.drawRect(0, 0, this.halfWidth, game.system.height);

            box.position.set(this.halfWidth, this.halfHeight);
            box.anchor = { x: 0.5, y: 0.5 };

            arrowRight.position.set(game.system.width - 50 * game.scale, this.halfHeight);
            arrowRight.anchor = { x: 0.5, y: 0.5 };

            arrowLeft.position.set(50 * game.scale, this.halfHeight);
            arrowLeft.anchor = { x: 0.5, y: 0.5 };
            arrowLeft.rotation = Math.PI;

            var tween1 = new game.Tween(rightRect);
            tween1.to({ alpha: 0.25 }, 3000);
            tween1.delay(1000);
            tween1.repeat();
            tween1.yoyo();
            tween1.start();

            var tween2 = new game.Tween(leftRect);
            tween2.to({ alpha: 0.25 }, 3000);
            tween2.delay(4000);
            tween2.repeat();
            tween2.yoyo();
            tween2.start();

            var tween3 = new game.Tween(box);
            tween3.to({ rotation: 2 * Math.PI }, 3000);
            tween3.delay(1500);
            tween3.repeat();
            tween3.yoyo();
            tween3.start();

            this.container.addChild(arrowRight);
            this.container.addChild(arrowLeft);
            this.container.addChild(box);
            this.container.addChild(rightRect);
            this.container.addChild(leftRect);
        },

        addPlusButton: function() {
            var plus = new game.Button('plus',
                30 * game.scale,
                30 * game.scale,
                this.container
            );

            plus.sprite.scale = { x: 0.9, y: 0.9 };
            plus.sprite.mouseover = this.scale.bind(plus, 1);
            plus.sprite.mouseout = this.scale.bind(plus, 0.9);

            plus.sprite.click = plus.sprite.tap = function() {
                game.vk.showInviteBox();
            };
        },

        scale: function(scale) {
            game.scene.addTween(this.sprite.scale, {
                x: scale, y: scale
            }, 100, {
                easing: 'Quadratic.Out'
            }).start();
        },

        remove: function() {
            game.scene.addTween(this.container, { alpha: 0 }, 300, {
                easing: 'Quadratic.Out',
                onComplete: function() {
                    game.scene.stage.removeChild(this.container);
                }.bind(this)
            }).start();
        }
    });
});
