game.module(
    'game.objects.button'
)
.body(function() {
    game.createClass('Button', {
        types: {
            pause: 'pauseBtn',
            unPause: 'unpauseBtn',
            restart: 'restartBtn',
            music: 'musicBtn',
            endRestart: 'endRestartBtn',
            endPlaces: 'endPlacesBtn',
            plus: 'plusBtn'
        },

        init: function(type, x, y, container) {
            this.x = x;
            this.y = y;

            this.type = type;
            this.container = container || game.scene.stage;

            this.createSprite();
        },

        createSprite: function() {
            this.sprite = new game.Sprite(this.types[this.type]);
            this.sprite.position.x = this.x;
            this.sprite.position.y = this.y;
            this.sprite.anchor = { x: 0.5, y: 0.5 };
            this.sprite.interactive = true;
            this.sprite.buttonMode = true;

            var bounds = this.sprite.getBounds();
            bounds.x = bounds.x + bounds.x / 2;
            bounds.y = bounds.y + bounds.y / 2;
            bounds.width = bounds.width + 15 * game.scale;
            bounds.height = bounds.height + 15 * game.scale;

            this.sprite.hitArea = bounds;

            this.container.addChild(this.sprite);
        },

        remove: function() {
            this.container.removeChild(this.sprite);
        },

        show: function() {
            this.container.addChild(this.sprite);
        }
    });
});
