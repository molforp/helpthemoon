game.module(
    'game.objects.box'
)
.body(function() {
    game.createClass('Box', {
        isLose: false,

        init: function() {
            this.width = this.height = 55 * game.scale;
            this.x = game.system.width / 2;
            this.y = game.system.height;
            this.rightLastPos = game.system.width - 8 * game.scale;
            this.leftLastPos = 8 * game.scale;
            this.groups = game.scene.collisionGroups;
            this.collisionAgainst = this.groups.moon | this.groups.star | this.groups.weight;

            this.createSprite();
            this.createBody();
            this.createShape();

            // Add object to scene, so it's update() function get's called every frame
            // TODO: remove in panda 2.0
            game.scene.addObject(this);
        },

        createSprite: function() {
            this.sprite = new game.Sprite('box', this.x, this.y, {
                anchor: { x: 0.5, y: 0.5 },
                offset: { x: 0, y: 0 }
            });

            game.scene.levelContainer.addChild(this.sprite);
        },

        // create physics body for sprite
        createBody: function() {
            this.body = new game.Body({
                position: [this.x, this.y],
                velocity: [0, 0],
                velocityLimit: [0, 0],
                mass: 3,
                angle: 0,
                angularVelocity: 0,
                angularForce: 0
            });

            // Add body to world
            game.scene.world.addBody(this.body);
        },

        // create shape for body
        createShape: function() {
            this.shape = new game.Rectangle(this.width, this.height);
            this.shape.collisionGroup = this.groups.box;
            this.shape.collisionMask = this.collisionAgainst;

            this.body.addShape(this.shape);
        },

        remove: function() {
            game.removeFromLevel(this);
        },

        moveBox: function(body) {
            body.position[0] += 60 * game.scale * game.system.delta * game.scene.dir;
            body.angle += 2 * game.scene.dir * game.system.delta;
            body.angularVelocity = 2 * game.scene.dir;
        },

        lose: function(argument) {
            game.scene.addTimer(500, game.removeFromLevel.bind(null, this));
        },

        update: function() {
            var sprite = this.sprite;
            var body = this.body;

            // start box velocity
            body.velocity[1] = -100 * game.scale;
            body.velocity[0] = 0;

            // update sprite position and angle
            sprite.position.x = body.position[0];
            sprite.position.y = body.position[1];
            sprite.rotation = body.angle;

            if (body.position[0] >= this.rightLastPos) {
                body.position[0] = this.rightLastPos;
            } else if (body.position[0] <= this.leftLastPos) {
                body.position[0] = this.leftLastPos;
            }

            game.scene.isStarted && game.scene.isMouseDown && this.moveBox(body);
            game.scene.isEnd && !this.isLose && this.lose()
        }
    });
});
