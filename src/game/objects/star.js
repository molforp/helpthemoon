game.module(
    'game.objects.star'
)
.body(function() {
    game.createClass('Star', {
        init: function(x, y) {
            this.x = x;
            this.y = y;
            this.width = 30 * game.scale;
            this.height = 29 * game.scale;
            this.loseDistance = 100 * game.scale;
            this.groups = game.scene.collisionGroups;
            this.collisionAgainst = this.groups.box | this.groups.moon;

            this.createSprite();
            this.createBody();
            this.createShape();
            this.addPushStarTimer();

            // TODO: remove in panda 2.0
            game.scene.addObject(this);
        },

        createSprite: function() {
            this.sprite = new game.Sprite('star', this.x, this.y, {
                anchor: { x: 0.5, y: 0.5 },
                offset: { x: 0, y: 0 }
            });

            game.scene.levelContainer.addChild(this.sprite);
        },

        createBody: function() {
            this.body = new game.Body({
                position: [this.x, this.y],
                velocity: [0, 0],
                // velocityLimit: [40, 40],
                mass: 1,
                angle: 0
            });

            game.scene.world.addBody(this.body);
        },

        createShape: function() {
            this.shape = new game.Rectangle(this.width, this.height);
            this.shape.collisionGroup = this.groups.star;
            this.shape.collisionMask = this.collisionAgainst;

            this.body.addShape(this.shape);
        },

        pushStar: function() {
            this.body.velocity[0] = Math.random(-15 * game.scale, 15 * game.scale);
        },

        addPushStarTimer: function() {
            this.pushStarTimer = game.scene.addTimer(1000, function() {
                this.pushStar();
            }.bind(this), true);
        },

        remove: function() {
            game.removeFromLevel(this);
            game.scene.removeTimer(this.pushStarTimer);
        },

        removeWithTween: function(argument) {
            game.scene.addTween(this.sprite, { alpha: 0 }, 400, {
                easing: 'Quadratic.Out',
                onComplete: this.remove.bind(this)
            }).start();
        },

        catchIt: function(score) {
            if (this.isCatched) return;

            game.audio.playSound('boip');
            game.scene.updateScore(score);

            this.showScore('+' + score);

            this.remove();
            this.isCatched = true;
        },

        showScore: function(score) {
            this.score = new game.BitmapText(score.toString(), { font: '18px font' });
            this.score.position.set(this.body.position[0], this.body.position[1]);

            game.scene.levelContainer.addChild(this.score);

            game.scene.addTween(this.score, { alpha: 0 }, 600, {
                easing: 'Quadratic.Out',
                onComplete: function() {
                    game.scene.levelContainer.removeChild(this.score);
                }
            }).start();
        },

        loseIt: function() {
            if (game.scene.isEnd || this.isLosed) return;

            var score = -5;

            this.isLosed = true;
            this.removeWithTween();

            if (game.STATS.SCORE !== 0) {
                game.scene.updateScore(score);
                this.showScore(score);
            }
        },

        update: function() {
            var sprite = this.sprite;
            var body = this.body;
            var x = sprite.position.x = body.position[0];
            var y = sprite.position.y = body.position[1];

            sprite.rotation = body.angle;

            if (game.scene.isEnd) {
                this.removeWithTween();
                this.score && game.scene.levelContainer.removeChild(this.score);
                return;
            }

            if (y > game.scene.box.sprite.y + this.loseDistance) {
                this.loseIt();
            } else if (body.overlaps(game.scene.box.body)) {
                this.catchIt(5);
            } else if (body.overlaps(game.scene.moon.body)) {
                this.catchIt(10);
            }
        }
    });
});
