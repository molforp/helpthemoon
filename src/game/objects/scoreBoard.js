game.module(
    'game.objects.scoreBoard'
)
.body(function() {
    game.createClass('ScoreBoard', {
        width: 200,
        height: 262,

        init: function(x, y) {
            game.audio.playSound('gameOver');

            this.x = game.system.width / 2;
            this.y = game.system.height / 2;
            this.container = new game.Container().addTo(game.scene.stage);
            this.container.position.set(0, 150 * game.scale);

            this.addScore();
            this.addButtons();
            game.isVk && this.addShareVkButton();

            game.scene.isNewBestScore && this.addNewScoreLabel();

            game.scene.addTween(this.container, {
                x: 0, y: 0
            }, 300, {
                easing: 'Back.Out'
            }).start();
        },

        addNewScoreLabel: function() {
            this.newScore = new game.Sprite('newScore');
            this.newScore.position.x = -this.newScore.width;
            this.newScore.position.y = this.y + 29 * game.scale;
            this.newScore.anchor = { x: 1, y: 0.5 };

            game.scene.addTween(this.newScore, {
                x: this.x - (this.hightScoreText.textWidth + 8 * game.scale) / 2
            }, 200, {
                easing: 'Quartic.In',
                delay: 200,
                onStart: function() {
                    game.audio.playSound('newScoreAdd');
                }
            }).start();

            this.container.addChild(this.newScore);
        },

        addScore: function() {
            this.gameOverText = new game.BitmapText(game.trans('gameOver'), { font: '45px font' });
            this.gameOverText.position.x = this.x - this.gameOverText.textWidth / 2;
            this.gameOverText.position.y = this.y - 130 * game.scale;

            this.scoreText = new game.BitmapText(game.trans('score') + ': ' + game.STATS.SCORE.toString(), { font: '35px font' });
            this.scoreText.position.x = this.x - this.scoreText.textWidth / 2;
            this.scoreText.position.y = this.y - 40 * game.scale;

            this.hightScoreText = new game.BitmapText(game.trans('best') + ': ' + game.STATS.BEST.toString(), { font: '35px font' });
            this.hightScoreText.position.x = this.x - this.hightScoreText.textWidth / 2;
            this.hightScoreText.position.y = this.y + 10 * game.scale;

            this.container.addChild(this.scoreText);
            this.container.addChild(this.hightScoreText);
            this.container.addChild(this.gameOverText);
        },

        addButtons: function() {
            var y = this.y + 120 * game.scale;
            var restart = new game.Button('endRestart', this.x - 70 * game.scale, y, this.container);
            // var places = new game.Button('endPlaces', this.x + 70 * game.scale, y, this.container);

            restart.sprite.scale = { x: 0.9, y: 0.9 };
            // places.sprite.scale = { x: 0.9, y: 0.9 };

            restart.sprite.click = restart.sprite.tap = function() {
                game.system.setScene('Main');
            };

            restart.sprite.mouseover = scale.bind(restart, 1);
            restart.sprite.mouseout = scale.bind(restart, 0.9);
            // places.sprite.mouseover = scale.bind(places, 1);
            // places.sprite.mouseout = scale.bind(places, 0.9);

            // places.sprite.click = places.sprite.tap = function() {
            //     game.showTopPlaces();
            // };

            function scale(scale) {
                game.scene.addTween(this.sprite.scale, {
                    x: scale, y: scale
                }, 100, {
                    easing: 'Quadratic.Out'
                }).start();
            }
        },

        addShareVkButton: function() {
            var share = new game.Sprite('shareBtn');

            share.anchor = { x: 0.5, y: 0.5 };
            share.position.set(this.x, game.system.height - 50 * game.scale);
            share.interactive = true;
            share.buttonMode = true;

            share.click = share.tap = function() {
                game.vk.uploadWallPhoto();
            };

            this.container.addChild(share);
        }
    });
});
