game.module(
    'game.objects.weight'
)
.body(function() {
    game.createClass('Weight', {
        init: function(x, y) {
            this.x = x;
            this.y = y;
            this.width = this.height = 40 * game.scale;
            this.groups = game.scene.collisionGroups;
            this.collisionAgainst = this.groups.box | this.groups.moon;

            this.createSprite();
            this.createBody();
            this.createShape();

            // TODO: remove in panda 2.0
            game.scene.addObject(this);
        },

        createSprite: function() {
            this.sprite = new game.Sprite('weight', this.x, this.y, {
                anchor: { x: 0.5, y: 0.5 },
                offset: { x: 0, y: 0 }
            });

            game.scene.levelContainer.addChild(this.sprite);
        },

        createBody: function() {
            this.body = new game.Body({
                position: [this.x, this.y],
                velocity: [0, 130 * game.scale],
                mass: 15,
                angle: 0
            });

            game.scene.world.addBody(this.body);
        },

        // create shape for body
        createShape: function() {
            this.shape = new game.Rectangle(this.width, this.height);
            this.shape.collisionGroup = this.groups.weight;
            this.shape.collisionMask = this.collisionAgainst;

            this.body.addShape(this.shape);
        },

        lose: function() {
            if (this.isLosed) return;

            game.scene.addTween(this.sprite, { alpha: 0 }, 500, {
                easing: 'Quadratic.Out',
                onComplete: this.remove.bind(this)
            }).start();

            this.isLosed = true;
        },

        update: function() {
            var sprite = this.sprite;
            var body = this.body;
            var half = this.shape.width / 2;

            // update sprite position and angle
            sprite.position.x = body.position[0];
            sprite.position.y = body.position[1];
            sprite.rotation = body.angle;

            var isOut = (sprite.position.x >= game.system.width + half) || (sprite.position.x <= 0 - half);

            if (sprite.position.y > game.scene.box.sprite.y + 100 * game.scale || isOut || game.scene.isEnd) {
                this.lose();
            }
        },

        remove: function() {
            game.removeFromLevel(this);
        }
    });
});
