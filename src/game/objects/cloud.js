game.module(
    'game.objects.cloud'
)
.body(function() {
    game.createClass('Cloud', {
        types: [
            {
                name: 'cloud1',
                width: 160,
                height: 100
            },
            {
                name: 'cloud2',
                width: 120,
                height: 85
            },
            {
                name: 'cloud3',
                width: 180,
                height: 100
            },
            {
                name: 'cloud4',
                width: 110,
                height: 80
            }
        ],

        init: function(y, type) {
            this.toRight = Math.random(-1, 1) > 0;
            this.type = this.types[Math.floor(Math.random(0, 4))] || this.params[0];
            this.velocityX = this.toRight ? Math.random(1, 2) : Math.random(-1, -2);
            this.half = this.type.width * game.scale / 2;
            this.x = this.toRight ? -this.half : game.system.width + this.half;
            this.y = y;

            this.createSprite();
            this.createBody();
            this.createShape();

            // TODO: remove in panda 2.0
            game.scene.addObject(this);
        },

        createSprite: function() {
            this.sprite = new game.Sprite(this.type.name, this.x, this.y, {
                anchor: { x: 0.5, y: 0.5 },
                offset: { x: 0, y: 0 }
            });

            game.scene.levelContainer.addChild(this.sprite);
        },

        createBody: function() {
            this.body = new game.Body({
                position: [this.x, this.y],
                velocity: [0, 0],
                mass: 0,
                angle: 0
            });

            game.scene.world.addBody(this.body);
        },

        // create shape for body
        createShape: function() {
            this.shape = new game.Rectangle(this.type.width * game.scale, this.type.height * game.scale);
            this.body.addShape(this.shape);
        },

        update: function() {
            var sprite = this.sprite;
            var body = this.body;

            body.position[0] += this.velocityX * game.scale;
            body.position[1]  -= 1 * game.system.delta * game.scale;

            // update sprite position and angle
            sprite.position.x = body.position[0];
            sprite.position.y = body.position[1];
            sprite.rotation = body.angle;

            // remove when cloud is out from viewport
            if ((this.toRight && sprite.position.x >= game.system.width + this.half) ||
                (!this.toRight && sprite.position.x <= 0 - this.half) || game.scene.isEnd) {
                this.remove();
            }
        },

        remove: function() {
            game.removeFromLevel(this);
        }
    });
});
