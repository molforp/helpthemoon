game.module(
    'game.objects.topButtons'
)
.body(function() {
    game.createClass('TopButtons', {
        init: function() {
            var isMusic = game.storage.get('music', true);
            isMusic || this.muteMusic();

            this.container = new game.Container().addTo(game.scene.stage);
            this.addMusicButton();
        },

        addMusicButton: function(argument) {
            var posOffset = 25 * game.scale;
            var button = new game.Button('music', game.system.width - posOffset, posOffset, this.container);
            var self = this;

            button.sprite.alpha = game.audio.musicMuted ? 0.5 : 1;
            button.sprite.click = button.sprite.tap = onClick;

            function onClick() {
                button.sprite.alpha = game.audio.musicMuted ? 1 : 0.5;

                self.isButtonClick = true;
                self.toggleMusic();
            }
        },

        addButtons: function() {
            var pause, unPause, restart;
            var posOffset = 25 * game.scale;

            pause = new game.Button('pause', posOffset, posOffset, this.container);

            pause.sprite.click = pause.sprite.tap = onPauseClick;

            function onPauseClick() {
                game.scene.addTimer(0, function() {
                    game.system.pause();
                }.bind(this));

                pause.remove();
                unPause ? unPause.show() : (unPause = new game.Button('unPause', posOffset, posOffset, this.container));
                restart ? restart.show() : (restart = new game.Button('restart', posOffset * 3, posOffset, this.container));

                unPause.sprite.click = unPause.sprite.tap = onUnPauseClick;
                restart.sprite.click = restart.sprite.tap = onRestartClick;

                game.audio.pauseMusic();
            }

            function onUnPauseClick() {
                game.system.resume();

                unPause.remove();
                restart.remove();

                pause.show();
                game.audio.resumeMusic();
            }

            function onRestartClick() {
                game.system.setScene('Main');
            }
        },

        toggleMusic: function() {
            game.audio.musicMuted ? this.unmuteMusic() : this.muteMusic();
            game.storage.set('music', !game.audio.musicMuted);
        },

        muteMusic: function() {
            game.audio.muteSound();
            game.audio.muteMusic();
        },

        unmuteMusic: function() {
            game.audio.unmuteSound();
            game.audio.unmuteMusic();
        },

        remove: function() {
            game.scene.stage.removeChild(this.container);
        }
    });
});
