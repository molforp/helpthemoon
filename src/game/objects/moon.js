game.module(
    'game.objects.moon'
)
.body(function() {
    game.createClass('Moon', {
        isOut: false,

        init: function() {
            this.radius = 15 * game.scale;
            this.x = game.system.width / 2;
            this.y = game.system.height / 2 + 120 * game.scale;
            this.pushMoonPower = 8 * game.scale;
            this.groups = game.scene.collisionGroups;
            this.collisionAgainst = this.groups.box | this.groups.star | this.groups.weight;
            this.world = game.scene.world;

            this.createSprite();
            this.createBody();
            this.createShape();
            this.addPushMoonTimer();

            // TODO: remove in panda 2.0
            game.scene.addObject(this);
            game.audio.playSound('bass');
        },

        createSprite: function() {
            this.sprite = new game.Sprite('moon', this.x, this.y, {
                anchor: { x: 0.5, y: 0.5 },
                offset: { x: 0, y: 0 }
            });

            game.scene.levelContainer.addChild(this.sprite);
        },

        createBody: function() {
            this.body = new game.Body({
                position: [this.x, this.y],
                velocity: [0, 200 * game.scale],
                mass: 4,
                angle: 0
            });

            game.scene.world.addBody(this.body);
        },

        createShape: function() {
            this.shape = new game.Circle(this.radius);
            this.shape.collisionGroup = this.groups.moon;
            this.shape.collisionMask = this.collisionAgainst;

            this.body.addShape(this.shape);
        },

        pushMoon: function() {
            this.body.velocity[0] = Math.random(-this.pushMoonPower, this.pushMoonPower);
        },

        addPushMoonTimer: function(time) {
            game.scene.addTimer(1000, function() {
                game.scene.addTimer(Math.random(600, 2000), this.pushMoon.bind(this));
            }.bind(this), true);
        },

        remove: function() {
            game.scene.addTween(this.sprite, { alpha: 0 }, 300, {
                easing: 'Quadratic.Out',
                onComplete: function() {
                    game.removeFromLevel(this);
                }.bind(this)
            }).start();
        },

        update: function() {
            var sprite = this.sprite;
            var body = this.body;
            var moonPos = body.position;
            var boxPos = game.scene.box.body.position;
            var isMoonBelowBox = moonPos[1] > boxPos[1];
            var isMoonOut = (moonPos[0] >= game.system.width + 3 * game.scale) || (moonPos[0] <= -3 * game.scale);

            // update sprite position and angle
            sprite.position.x = body.position[0];
            sprite.position.y = body.position[1];
            sprite.rotation = body.angle;

            if (this.isOut) return;

            if (isMoonBelowBox || isMoonOut) {
                game.scene.gameOver();

                body.velocity[1] = 600;

                game.scene.addTimer(1000, function() {
                    this.remove();
                }.bind(this))

                this.isOut = true;
            }
        }
    });
});
