module.exports = {
    name: 'Help The Moon',
    version: '0.0.1',

    system: {
        width: 320,
        height: 480,
        retina: true,
        hires: 4,
        scaleToFit: true,
        resizeToFill: true,
        rotateScreen: false
    },

    storage: {
        id: 'htm'
    },

    canvasId: 'canvas'
};
