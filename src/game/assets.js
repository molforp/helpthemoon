game.module(
    'game.assets'
)
.body(function() {
    game.addAsset('logo.png', 'logo');
    game.addAsset('box.jpg', 'box');
    game.addAsset('star.png', 'star');
    game.addAsset('moon.png', 'moon');
    game.addAsset('cloud1.png', 'cloud1');
    game.addAsset('cloud2.png', 'cloud2');
    game.addAsset('cloud3.png', 'cloud3');
    game.addAsset('cloud4.png', 'cloud4');
    game.addAsset('weight.png', 'weight');
    game.addAsset('bg.png', 'bg');
    game.addAsset('newScore.png', 'newScore');
    game.addAsset('pauseBtn.png', 'pauseBtn');
    game.addAsset('unpauseBtn.png', 'unpauseBtn');
    game.addAsset('musicBtn.png', 'musicBtn');
    game.addAsset('restartBtn.png', 'restartBtn');
    game.addAsset('plusBtn.png', 'plusBtn');
    game.addAsset('endRestartBtn.png', 'endRestartBtn');
    game.addAsset('endPlacesBtn.png', 'endPlacesBtn');
    game.addAsset('shareBtn.png', 'shareBtn');
    game.addAsset('howTo.png', 'howTo');
    game.addAsset('arrowRight.png', 'arrowRight');
    game.addAsset('boxHowTo.png', 'boxHowTo');
    game.addAsset('moonSpriteDown.json', 'moonSpriteDown');

    game.addAsset('font.fnt');

    game.addAudio('audio/bass.m4a', 'bass');
    game.addAudio('audio/boip.m4a', 'boip');
    game.addAudio('audio/congab.m4a', 'congab');
    game.addAudio('audio/explo3.m4a', 'explo3');
    game.addAudio('audio/gameOver.m4a', 'gameOver');
    game.addAudio('audio/highDown.m4a', 'highDown');
    game.addAudio('audio/newScore.m4a', 'newScoreAdd');
});
