game.module(
    'game.main'
)
.require(
    'game.assets',
    'game.platform',
    'game.objects.moon',
    'game.objects.box',
    'game.objects.star',
    'game.objects.cloud',
    'game.objects.weight',
    'game.objects.button',
    'game.objects.scoreBoard',
    'game.objects.topButtons',
    'game.objects.startPanel',
    'plugins.p2',
    'plugins.translator'
)
.body(function() {
    game.isVk = game.PLATFORM === 'vk';
    game.isWeb = game.PLATFORM === 'web';

    game.storage = new game.Storage();

    game.STATS = {};
    game.LANGS = {
        en: {
            best: 'BEST',
            score: 'SCORE',
            gameOver: 'GAME OVER',
            or: 'or'
        },

        ru: {
            best: 'ЛУЧШИЙ',
            score: 'СЧЕТ',
            gameOver: 'ИГРА ОКОНЧЕНА',
            or: 'или'
        }
    };

    game.removeFromLevel = function(obj) {
        game.scene.removeObject(obj);
        game.scene.levelContainer.removeChild(obj.sprite);
        game.scene.world.removeBody(obj.body);
    };

    game.showTopPlaces = function() {
        console.log('top');
    };

    game.showStartPanel = function() {
        game.startPanel.addBest();
    };

    game.createScene('Main', {
        isStarted: false,
        isEnd: false,
        gravity: 20,
        isMusic: null,
        isButtonClick: false,
        tap: 0,
        dir: 0, // box move direction

        collisionGroups: {
            moon: Math.pow(2, 0),
            box: Math.pow(2, 1),
            star: Math.pow(2, 2),
            weight: Math.pow(2, 3)
        },

        init: function() {
            window.focus(); // TODO: for VK iframe

            game.audio.musicVolume = 0.1;


            var self = this;
            game.STATS.SCORE = 0;
            game.STATS.BEST = 0;
            // game.STATS.BEST = game.storage.get('best', 0);

            this.world = new game.World({ gravity: [0, this.gravity] });
            this.startLeftPos = 50 * game.scale;
            this.startRightPos = game.system.width - 50 * game.scale;

            this.addLevelBackground();
            this.levelContainer = new game.Container().addTo(this.stage);
            this.addClickContainerRight();
            this.addClickContainerLeft();

            this.topButtons = new game.TopButtons();
            game.startPanel = new game.StartPanel();

            game.getBestScore();
        },

        startGame: function() {
            if (this.isStarted) return;

            game.startPanel.remove();
            this.isStarted = true;

            this.moon = new game.Moon();
            this.box = new game.Box();

            this.addCamera();
            this.addScore();
            this.startClouds();
            this.startStars();
            this.topButtons.addButtons();
        },

        addLevelBackground: function() {
            var bg = new game.Container().addTo(this.stage);
            var levelBg = new game.TilingSprite('bg');

            bg.position.set(0, 0);

            levelBg.speed.x = -3 * game.scale;
            levelBg.speed.y = 80 * game.scale;
            levelBg.position.set(0, 0); // Place the background in the centre of the screen
            bg.addChild(levelBg); // Add the background to the bg container

            game.scene.addObject(levelBg);
        },

        addClickContainerRight: function() {
            this.clckContRight = new game.Container().addTo(this.stage);
            this.clckContRight.interactive = true;
            this.clckContRight.buttonMode = true;
            this.clckContRight.hitArea = new game.HitRectangle(game.system.width / 2, 0, game.system.width / 2, game.system.height);

            this.clckContRight.mousedown = this.clckContRight.touchstart = function() {
                this.onLevelClick('right');
            }.bind(this);

            this.clckContRight.mouseup = this.clckContRight.touchend = function() {
                this.setUnTap();
            }.bind(this);
        },

        addClickContainerLeft: function() {
            this.clckContLeft = new game.Container().addTo(this.stage);
            this.clckContLeft.interactive = true;
            this.clckContLeft.buttonMode = true;
            this.clckContLeft.hitArea = new game.HitRectangle(0, 0, game.system.width / 2, game.system.height);

            this.clckContLeft.mousedown = this.clckContLeft.touchstart = function() {
                this.onLevelClick('left');
            }.bind(this);

            this.clckContLeft.mouseup = this.clckContLeft.touchend = function() {
                this.setUnTap();
            }.bind(this);
        },

        keydown: function(key) {
            this.setTap();

            if (key === 'LEFT') {
                this.dir = -1;
            } else if (key === 'RIGHT') {
                this.dir = 1;
            }
        },

        keyup: function() {
            this.setUnTap();
        },

        setTap: function() {
            this.tap--;
            this.isMouseDown = true;
        },

        setUnTap: function(argument) {
            this.tap > 1 || this.tap++;

            this.isMouseDown = !this.tap;
        },

        addScore: function() {
            this.scoreText = new game.BitmapText(game.STATS.SCORE.toString(), { font: '35px font' });
            this.updateScorePos();

            game.scene.stage.addChild(this.scoreText);
        },

        updateScore: function(score) {
            game.STATS.SCORE += score;

            this.scoreText.setText(game.STATS.SCORE.toString());
            this.scoreText.updateTransform();
            this.updateScorePos();
        },

        updateScorePos: function() {
            this.scoreText.position.x = game.system.width / 2 - this.scoreText.textWidth / 2;
            this.scoreText.position.y = 40 * game.scale - this.scoreText.textHeight / 2;
        },

        addCamera: function() {
            var camera = new game.Camera();

            camera.addTo(this.levelContainer);
            camera.acceleration = 5;
            camera.minX = camera.maxX = 0;
            camera.maxX = 0;
            camera.offset.y = game.system.height - 50 * game.scale;
            camera.setTarget(this.box.sprite);

            this.camera = camera;
        },

        startStars: function() {
            this.addStarTimer = game.scene.addTimer(2500, function() {
                new game.Star(this.randomPosX(), this.camera.position.y - 100 * game.scale);

                this.addWeight();
            }.bind(this), true);
        },

        addWeight: function() {
            Math.random(0, 6) > 4 && new game.Weight(this.randomPosX(), this.camera.position.y - 100 * game.scale);
        },

        startClouds: function() {
            this.addCloudTimer = game.scene.addTimer(5000, function() {
                new game.Cloud(this.camera.position.y + Math.random(100, 200) * game.scale);
            }.bind(this), true);
        },

        randomPosX: function() {
            var min = this.startLeftPos;
            var max = this.startRightPos;

            return Math.floor(Math.random() * (max - min + 1)) + min;
        },

        onLevelClick: function(pos) {
            if (this.isStarted && !game.system.paused) {
                this.dir = pos === 'left' ? -1 : 1;
                this.setTap();
            } else {
                game.scene.addTimer(200, function() {
                    this.startGame();
                }.bind(this));
            }
        },

        showScore: function() {
            this.scoreBoard = new game.ScoreBoard();
            this.topButtons.remove();
        },

        gameOver: function() {
            game.audio.stopMusic();
            game.audio.playSound('highDown');

            if (game.STATS.SCORE > game.STATS.BEST) {
                console.log('OLD: ' + game.STATS.BEST, ', NEW: ' + game.STATS.SCORE);
                game.STATS.BEST = game.STATS.SCORE;
                this.isNewBestScore = true;
                game.updateStorage(game.STATS.BEST);
            }

            this.isEnd = true;
            this.timers.length = 0;

            game.tweenEngine.removeAll();

            for (var i = 0; i < this.objects.length; i++) {
                if (this.objects[i].tilePosition) this.objects[i].speed.y = 0;
            }

            this.addTimer(500, this.showScore.bind(this));
        },

        mousedown: function() {
            window.focus();
        }
    });
});
