var gulp = require('gulp');
var config = require('./config.js');
var exec = require('child_process').exec;
var $ = require('gulp-load-plugins')();
var glog = $.util.log;
var colors = $.util.colors;

gulp.task('build:media', function() {
    return gulp.src(config.paths.media)
        .pipe(gulp.dest(config.paths.buildPath + '/media'));
});

gulp.task('build:copyMin', function() {
    return gulp.src('game.min.js')
        .pipe(gulp.dest(config.paths.buildPath));
});

gulp.task('build:css', function() {
    return gulp.src(config.paths.currenPlatformPath + '/style.css')
        .pipe($.minifyCss())
        .pipe(gulp.dest(config.paths.buildPath));
});

gulp.task('build:index', ['build:copyMin', 'build:css', 'build:media'], function() {
    return gulp.src(config.paths.currenPlatformPath + '/index.html')
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true,
            conditionals: true
        }))
        .pipe(gulp.dest(config.paths.buildPath));
});

gulp.task('build:preBuild', function() {
    return gulp.src(config.paths.currenPlatformPath + '/platform.js')
        .pipe(gulp.dest(config.paths.gamePath));
});

gulp.task('build', ['build:preBuild', 'gameConf'], function(cb) {
    if (!config.platformFromParam) {
        glog(colors.magenta('Platform is not specified, using default platform:'), colors.blue(config.currentPlatform));
    }

    glog(colors.green('BUILD:'), colors.blue(config.currentPlatform), colors.yellow('(v' + config.platformVersion + ')'));

    exec('panda build', function(err, stdout, stderr) {
        cb(err);
        glog(colors.green('BUILD:'), colors.blue('done!'));
        gulp.start('build:index');
    });
});
