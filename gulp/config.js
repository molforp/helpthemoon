var $ = require('gulp-load-plugins')();
var platformFromParam = $.util.env.p || $.util.env.platform;
var platform = platformFromParam || 'web';

var config = {
    platforms: ['web', 'vk', 'ios'],
    platformFromParam: platformFromParam,
    currentPlatform: platform,
    platformVersion: '0.0.0',
    paths: {
        builds: './builds',
        media: 'media/**',
        platformsPath: './src/platforms',
        gamePath: './src/game',
        platformFile: ''
    },

    divShotConfig: {
        name: 'htm',
        root: 'builds',
        clean_urls: true,
        cache_control: {},
        routes: {
            '**': 'index.html'
        }
    }
};

config.paths.buildPath = config.paths.builds + '/' + config.currentPlatform;
config.paths.currenPlatformPath = config.paths.platformsPath + '/' + config.currentPlatform;
config.paths.versionPath = config.paths.platformsPath + '/' + config.currentPlatform + '/version.txt';

module.exports = config;
