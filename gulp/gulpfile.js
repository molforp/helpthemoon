var gulp = require('gulp');
var fs = require('fs');
var $ = require('gulp-load-plugins')();
var config = require('./config.js');

$.del = require('del');

gulp.task('clean', function(cb) {
    $.del([config.paths.builds], cb);
});

gulp.task('gameConf', function(cb) {
    var gameConfig = require('../src/game/defaultConfig.js');
    gameConfig.version = config.platformVersion;

    fs.writeFile(config.paths.gamePath + '/config.js', 'pandaConfig = ' + JSON.stringify(gameConfig) + ';', function(err) {
        if (err) return cb(err);
        cb();
    });
});

require('./server.js');
require('./build.js');
require('./release.js');
require('./push.js');
