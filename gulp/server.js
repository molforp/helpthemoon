var gulp = require('gulp');
var browserSync = require('browser-sync');
var config = require('./config.js');

function getPlatformRoutes() {
    var platformRoutes = {};

    config.platforms.forEach(function(platform) {
        platformRoutes['/' + platform] = 'src/platforms/' + platform + '/dev.html';
    });

    return platformRoutes;
}

gulp.task('start', ['gameConf'], function() {
    browserSync({
        files: [
            config.paths.gamePath + '/**',
            config.paths.platformsPath + '/**/dev.html',
            config.paths.platformsPath + '/**/style.css',
            config.paths.platformsPath + '/**/platform.js'
        ],
        open: false,
        ghostMode: false,
        notify: false,
        port: 9000,
        reloadOnRestart: true,
        server: {
            baseDir: process.cwd(),
            routes: getPlatformRoutes()
        }
    });
});
