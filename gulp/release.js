var gulp = require('gulp');
var fs = require('fs');
var exec = require('child_process').exec;
var $ = require('gulp-load-plugins')();
var glog = $.util.log;
var colors = $.util.colors;
var env = $.util.env;
var config = require('./config.js');

function getNewVersion(ver) {
    var version = ver.split('.');
    var newVer = env.ver || env.v;
    var isMajor = env.major || env.ma;
    var isMinor = env.minor || env.mi;

    function increment(ver) {
        return parseInt(ver, 10) + 1
    }

    if (newVer) {
        return newVer;
    } else if (isMajor) {
        version[0] = increment(version[0]);
        version[1] = version[2] = 0;
    } else if (isMinor) {
        version[1] = increment(version[1]);
        version[2] = 0;
    } else {
        version[2] = increment(version[2]);
    }

    return version.join('.');
}

gulp.task('release:commitRelease', ['build'], function(cb) {
    var commitMsg = '"RELEASE: ' + config.currentPlatform + ' (v' + config.platformVersion + ')"';

    glog(colors.green('RELEASE:'), colors.blue(config.currentPlatform), colors.yellow('(v' + config.platformVersion + ')'));

    exec('git commit ' + config.paths.versionPath + ' -m ' + commitMsg, function(err, stdout, stderr) {
        console.log(stdout);

        glog(colors.green('RELEASE:'), colors.blue('Pushing to repository...'));

        exec('git push origin master', function(err, stdout, stderr) {
            cb(err);
            glog(colors.green('RELEASE:'), colors.blue('done!'));
        });
    });
});

gulp.task('release', function() {
    if (!config.platformFromParam) {
        throw colors.red('Platform is not specified');
    }

    if (config.platforms.indexOf(config.platformFromParam) === -1) {
        throw colors.red('Platform does not exist');
    }

    gulp.start('release:build');
});

gulp.task('release:changeVersion', function(cb) {
    var versionPath = config.paths.versionPath;

    // get old version from version.txt
    fs.readFile(versionPath, 'utf-8', function(err, oldVersion) {
        config.platformVersion = getNewVersion(oldVersion || config.platformVersion);

        // write new version to version.txt
        fs.writeFile(versionPath, config.platformVersion, function(err) {
            if (err) return cb(err);
            cb();
        });
    });
});

gulp.task('release:build', ['release:changeVersion'], function() {
    gulp.start('release:commitRelease');
});
