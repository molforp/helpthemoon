var gulp = require('gulp');
var config = require('./config.js');
var exec = require('child_process').exec;
var bb = require('bitballoon');
var $ = require('gulp-load-plugins')();
var isProd = $.util.env.production || $.util.env.prod;
var isDev = $.util.env.development || $.util.env.dev;
var ENV = isProd ? 'production' : isDev ? 'development' : 'development';
var glog = $.util.log;
var colors = $.util.colors;

$.del = require('del');

gulp.task('divshotPush', ['divshotConf'], function(cb) {
    exec('divshot push ' + ENV, function(err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        $.del('divshot.json');
        cb(err);
    });
});

gulp.task('divshotConf', function() {
    config.divShotConfig.root = config.paths.buildPath;
    config.divShotConfig.name = config.divShotConfig.name + '-' + config.currentPlatform;

    return $.file('divshot.json', JSON.stringify(config.divShotConfig), { src: true })
        .pipe(gulp.dest('.'));
});

gulp.task('push:vk', function() {
    // gulp.start('divshotPush');
    bb.deploy({
        access_token: 'ce7b5af68e79e0a6d91ae7ebf399b23405d6f57477908f377533d355f44605a2',
        site_id: '3e286488-6e3a-4f8f-9ac1-6ce4c7952db7',
        dir: 'builds/vk'
    }, function(err, deploy) {
        if (err) { throw(err) }
    });
});

gulp.task('push:ios', ['build'], function() {
    bb.deploy({
        access_token: 'af58886f488a3365e00fdb48b151e83cad1642d09c3511f330d1aa9dc78ac4c0',
        site_id: 'e885a4bf-3acb-4128-af2f-cf5033d49ac9',
        dir: 'builds/ios'
    }, function(err, deploy) {
        if (err) { throw(err) };
        glog(colors.green('PUSH:'), colors.blue('done!'));
    });
});

gulp.task('push:web', function() {
    gulp.start('divshotPush');
});

gulp.task('push', function() {
    if (!config.platformFromParam) {
        throw colors.red('Platform is not specified');
    }

    if (config.platforms.indexOf(config.platformFromParam) === -1) {
        throw colors.red('Platform does not exist');
    }

    glog(colors.green('PUSH:'), colors.blue(config.currentPlatform), colors.yellow('(' + ENV + ')'));

    gulp.start('push:' + config.currentPlatform);
});
